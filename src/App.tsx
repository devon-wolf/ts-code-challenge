import React from 'react';
import './App.css';
import EpisodePage from './pages/EpisodePage';

function App() {
  return (
    <div className="App">
      <EpisodePage />
    </div>
  );
}

export default App;
