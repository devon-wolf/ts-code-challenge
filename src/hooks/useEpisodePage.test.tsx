import React from 'react';
import { renderHook } from '@testing-library/react-hooks';
import { useEpisodePage } from './useEpisodePage';

describe('useEpisodePage', () => {
  it('exists', () => {
    expect(useEpisodePage).toBeTruthy();
  });
  it('can be rendered in a component', () => {
    const { result } = renderHook(() => useEpisodePage());
    expect(result).toBeTruthy();
  });
  it('returns an array of episodes and a loading state', () => {
    const { result } = renderHook(() => useEpisodePage());
    expect(result.current.episodes).toEqual(expect.any(Array));
    expect(result.current.loading).toEqual(expect.any(Boolean));
  });
});
