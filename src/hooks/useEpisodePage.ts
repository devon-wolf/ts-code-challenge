import { useEffect, useState } from 'react';
import { MagnusEpisodeResponse } from '../types';
import { getMagnusEpisodes } from '../utils/apiUtils';

export const useEpisodePage = () => {
  const [episodes, setEpisodes] = useState<MagnusEpisodeResponse[]>([]);
  const [loading, setLoading] = useState(false);
  const handleFetch = async (): Promise<void> => {
    setLoading(true);
    const { data } = await getMagnusEpisodes();
    setEpisodes(data);
    setLoading(false);
  };

  useEffect(() => {
    handleFetch();
  }, []);

  return { episodes, loading };
};
