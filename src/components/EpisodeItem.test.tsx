import React from 'react';
import EpisodeItem, { EpisodeItemProps } from './EpisodeItem';
import { render } from '@testing-library/react';
import { magnusEpisodeList } from '../constants/testConstants';

describe('EpisodeItem', () => {
  const defaultProps: EpisodeItemProps = { episode: magnusEpisodeList[0] };
  it('renders successfully', () => {
    const { baseElement } = render(<EpisodeItem {...defaultProps} />);
    expect(baseElement).toBeTruthy();
  });
});
