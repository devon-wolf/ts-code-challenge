import React from 'react';
import { MagnusEpisodeResponse } from '../types';

export interface EpisodeItemProps {
  episode: MagnusEpisodeResponse;
}
const EpisodeItem = ({ episode }: EpisodeItemProps) => {
  return <li>{episode.title}</li>;
};

export default EpisodeItem;
