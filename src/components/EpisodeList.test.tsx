import React from 'react';
import EpisodeList, { EpisodeListProps } from './EpisodeList';
import { render } from '@testing-library/react';
import { magnusEpisodeList } from '../constants/testConstants';

describe('EpisodeList', () => {
  const defaultProps: EpisodeListProps = {
    episodes: magnusEpisodeList,
    listTitle: 'Test Title',
  };
  it('renders successfully', () => {
    const { baseElement } = render(<EpisodeList {...defaultProps} />);
    expect(baseElement).toBeTruthy();
  });
});
