import React from 'react';
import { MagnusEpisodeResponse } from '../types';
import EpisodeItem from './EpisodeItem';

export interface EpisodeListProps {
  episodes: MagnusEpisodeResponse[];
  listTitle: string;
}

const EpisodeList = ({ episodes, listTitle }: EpisodeListProps) => {
  return (
    <section>
      <h1>{listTitle}</h1>
      <ul>
        {episodes.length > 0 ? (
          episodes.map((episode) => (
            <EpisodeItem key={episode.id} episode={episode} />
          ))
        ) : (
          <p>No episodes found</p>
        )}
      </ul>
    </section>
  );
};

export default EpisodeList;
