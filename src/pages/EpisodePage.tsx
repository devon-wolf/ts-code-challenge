import EpisodeList from '../components/EpisodeList';
import { useEpisodePage } from '../hooks/useEpisodePage';

const EpisodePage = () => {
  const { episodes, loading } = useEpisodePage();

  return (
    <main>
      {loading ? (
        <p role="progressbar">Loading!</p>
      ) : (
        <EpisodeList
          {...{
            episodes,
            listTitle: 'Magnus Archives Episodes in No Particular Order',
          }}
        />
      )}
    </main>
  );
};

export default EpisodePage;
