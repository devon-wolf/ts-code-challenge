import React from 'react';
import EpisodePage from './EpisodePage';
import { render, screen } from '@testing-library/react';
import * as hooks from '../hooks/useEpisodePage';
import { mockMagnusResponse } from '../constants/testConstants';

describe('EpisodePage', () => {
  const loadingState = {
    episodes: [],
    loading: true,
  };
  const populatedState = {
    episodes: mockMagnusResponse.data,
    loading: false,
  };
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('renders successfully', () => {
    const { baseElement } = render(<EpisodePage />);
    expect(baseElement).toBeTruthy();
  });
  it('renders a loading indicator if loading', () => {
    jest.spyOn(hooks, 'useEpisodePage').mockImplementation(() => loadingState);
    render(<EpisodePage />);
    expect(screen.getByRole('progressbar')).toBeTruthy();
  });
  it('renders a list of episodes when loaded', () => {
    jest
      .spyOn(hooks, 'useEpisodePage')
      .mockImplementation(() => populatedState);
    render(<EpisodePage />);
    expect(screen.getByRole('list')).toBeTruthy();
  });
});
