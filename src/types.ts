export interface PricedItem {
  id: string;
  price: number;
  quantity: number;
}

export interface ItemTotals {
  totalPrice: number;
  totalQuantity: number;
}

export interface MagnusEpisodeListResponse {
  count: string;
  description: string;
  data: MagnusEpisodeResponse[];
}

export interface MagnusEpisodeResponse {
  id: string;
  episodeNumber: number;
  title: string;
  season: number;
  releaseDate: Date | string;
  official: boolean;
  transcript: string;
}

export interface MagnusSeasonSummary {
  season: number;
  episodeCount: number;
}
