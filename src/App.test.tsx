import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

it('renders successfully', () => {
  const { baseElement } = render(<App />);
  expect(baseElement).toBeTruthy();
});
