import { getEpisodesBySeason, getSeasonSummary } from './seasonUtils';
import { magnusEpisodeList, magnusSeason1 } from '../constants/testConstants';

describe('season utilities', () => {
  describe('getSeasonSummary', () => {
    it('takes in an array of episodes and a season number and returns a summary of the indicated season', () => {
      const expected = {
        season: 1,
        episodeCount: 47,
      };
      const actual = getSeasonSummary(magnusEpisodeList, 1);
      expect(actual).toEqual(expected);
    });
  });
  describe('getEpisodesBySeason', () => {
    it('takes in an array of episodes and a season number and returns an array of episodes for that season', () => {
      const expected = magnusSeason1;
      const actual = getEpisodesBySeason(magnusEpisodeList, 1);
      expect(actual).toEqual(expected);
    });
  });
});
