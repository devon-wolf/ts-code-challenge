import { ItemTotals, PricedItem } from '../types';

export function getTotals(items: PricedItem[]): ItemTotals {
  return items.reduce(
    (prev, current) => {
      return {
        totalPrice: prev.totalPrice + current.price,
        totalQuantity: prev.totalQuantity + current.quantity,
      };
    },
    { totalPrice: 0, totalQuantity: 0 }
  );
}
