import { MagnusEpisodeResponse, MagnusSeasonSummary } from '../types';

export function getSeasonSummary(
  episodes: MagnusEpisodeResponse[],
  season: number
): MagnusSeasonSummary {
  const seasonEpisodes = getEpisodesBySeason(episodes, season);
  return { season, episodeCount: seasonEpisodes.length };
}

export function getEpisodesBySeason(
  episodes: MagnusEpisodeResponse[],
  season: number
): MagnusEpisodeResponse[] {
  return episodes.filter((episode) => episode.season === season);
}
