import { getTotals } from './pricedItemUtils';

describe('priced item utils', () => {
  it('returns the total price and quantity of an array of items', () => {
    const testItems = [
      {
        id: '1',
        price: 100,
        quantity: 1,
      },
      {
        id: '2',
        price: 300,
        quantity: 7,
      },
      {
        id: '3',
        price: 100,
        quantity: 2,
      },
    ];
    const expected = { totalPrice: 500, totalQuantity: 10 };
    const actual = getTotals(testItems);
    expect(actual).toEqual(expected);
  });
});
