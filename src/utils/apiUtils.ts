import { MagnusEpisodeListResponse } from '../types';

export async function getMagnusEpisodes(): Promise<MagnusEpisodeListResponse> {
  const API_URL = `https://magnus-archive.herokuapp.com/episodes`;
  try {
    const response = await fetch(API_URL);
    const json = (await response.json()) as MagnusEpisodeListResponse;
    return json;
  } catch (error) {
    console.error(error);
    throw error;
  }
}
