import { getMagnusEpisodes } from './apiUtils';
import { mockMagnusResponse } from '../constants/testConstants';

describe('api utils', () => {
  it('returns a response from the API', async () => {
    // global.fetch mock based on this very helpful guide: https://benjaminjohnson.me/mocking-fetch
    const mockFetch = jest.spyOn(global, 'fetch').mockReturnValue(
      Promise.resolve({
        json: () => Promise.resolve(mockMagnusResponse),
      }) as Promise<Response>
    );
    const expected = mockMagnusResponse;
    const actual = await getMagnusEpisodes();
    expect(mockFetch).toHaveBeenCalled();
    expect(actual).toEqual(expected);
  });
});
