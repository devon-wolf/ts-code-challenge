- add React Testing Library and Jest
  - (included in `create-react-app`)
- create a function that takes in an array of objects and returns a single object that has a type
  - `getSeasonSummary`
- create a function that transforms an array of objects into a data model of your choice using types/interfaces to pass tests
  - `getSeasonSummary`
- create a function that filters an array, only returning an array of objects that match a given criteria, while adhering to interface/type
  - `getEpisodesBySeason`
- create a function that returns the total amount (price and quantity) of a given array of objects
  - `getTotals`

---

- create an async function that gets data from an API
  - `getMagnusEpisodes`
- write 2 React functional components - one for displaying a single element via props, the other taking an array as a prop and mapping through it to display elements in a `ul`
  - `EpisodeList` and `EpisodeItem`
- write a React container component with hooks, combining an async function that hits a third party API and the presentational components from the previous step to display a list; app should be unit tested and have no compiler errors; a PR should be sent for review
  - `EpisodePage`
